<?php
//require_once './vendor/autoload.php'
//use 10quality/php-css-color-parser;

/*
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
//*/

/**
Entry point for a pathway viewer widget that can be included in other pages.

Example: https://tools.wmflabs.org/pathway-viewer/?id=WP3678&F00=Uniprot-TrEMBL_P09544&yellow=wikidata_Q14901827,Ensembl_ENSG00000148400

This page will display the interactive pathway viewer for a given pathway. It takes the following parameters:
- identifier: the pathway identifier (e.g. WP4)
- version: the version (revision) number of a specific version of the pathway (optional, leave out to display the newest version)

You can include a pathway viewer in another website using an iframe:

<iframe src="http://www.wikipathways.org/pathways/WP4?view=widget" width="500" height="500" style="overflow:hidden;"></iframe>
*/

/*
New format:
http://www.wikipathways.org/pathways/WP4?view=widget&<color>=<target>[,<target>...][&<color>=<target>[,<target>...]][&version=<VersionNumber>]

where:
	color: a name, e.g., red, or a hexadecimal, e.g., FF0000 (no "#")
	target can be any of the following:
		Entity ID, ie., GraphId in GPML
		<XrefDataSource>:<XrefId> for a DataNode Xref
			XrefDataSource:
				BridgeDb conventional name specified by the pathway author,
					e.g., Ensembl, Entrez Gene, HMDB, etc.
				ensembl (GeneProducts only)
				ncbigene (GeneProducts only)
				wikidata
			XrefId: the identifier for a DataNode Xref, e.g., 1234
		Entity Type, e.g., DataNode, Metabolite, Interaction, Mitochondria, or
			one of the other words from the GPML or WP vocabs.
		TextContent: the name or label of the entity (URL-encoded)

http://www.wikipathways.org/pathways/WP4?view=widget
http://www.wikipathways.org/pathways/WP4?view=widget&version=7772
http://www.wikipathways.org/pathways/WP4?green=CRH&blue=ncbigene:8525&view=widget
http://www.wikipathways.org/pathways/WP4?green=APC&blue=HMDB:HMDB00193&view=widget
http://www.wikipathways.org/pathways/WP4?green=APC&red=TP53&blue=ATP&view=widget
http://www.wikipathways.org/pathways/WP4?purple=Entrez Gene:324,HMDB:HMDB00193&view=widget
http://www.wikipathways.org/pathways/WP4?purple=Entrez Gene:324,HMDB:HMDB00193&version=7772&view=widget
http://dev.wikipathways.org/wpi/PathwayWidget.php?id=WP710&purple=PC,Entrez Gene:324,HMDB:HMDB00193
*/

# from https://www.php.net/manual/en/function.readfile.php#52598
function readfile_chunked($filename,$retbytes=true) {
    $chunksize = 1*(1024*1024); // how many bytes per chunk
    $buffer = '';
    $cnt =0;
    // $handle = fopen($filename, 'rb');
    $handle = fopen($filename, 'rb');
    if ($handle === false) {
        return false;
    }
    while (!feof($handle)) {
        $buffer = fread($handle, $chunksize);
        echo $buffer;
        if ($retbytes) {
            $cnt += strlen($buffer);
        }
    }
        $status = fclose($handle);
    if ($retbytes && $status) {
        return $cnt; // return num. bytes delivered like readfile() does.
    }
    return $status;

}

// see https://snipplr.com/view/68083/
// and https://stackoverflow.com/a/43706299
function validate_html_color($color) {
  /* Validates hex color, adding #-sign if not found. Checks for a Color Name first to prevent error if a name was entered.
  *   $color: the color hex value stirng to Validates
  */
 
  $result = 0;

  $named = array('aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'beige', 'bisque', 'black', 'blanchedalmond', 'blue', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate', 'coral', 'cornflowerblue', 'cornsilk', 'crimson', 'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod', 'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslateblue', 'darkslategray', 'darkturquoise', 'darkviolet', 'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite', 'forestgreen', 'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'greenyellow', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory', 'khaki', 'lavender', 'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral', 'lightcyan', 'lightgoldenrodyellow', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue', 'lightslategray', 'lightsteelblue', 'lightyellow', 'lime', 'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid', 'mediumpurple', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise', 'mediumvioletred', 'midnightblue', 'mintcream', 'mistyrose', 'moccasin', 'navajowhite', 'navy', 'oldlace', 'olive', 'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'seashell', 'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'snow', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'white', 'whitesmoke', 'yellow', 'yellowgreen');
 
  $normalized_name_candidate = strtolower($color);
  if (in_array($normalized_name_candidate, $named)) {
      /* A color name was entered instead of a Hex Value, so just exit function */
      $result=$normalized_name_candidate;
  } else if (preg_match('/^(\#[\da-f]{3}|\#[\da-f]{6}|rgba\(((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*,\s*){2}((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*)(,\s*(0\.\d+|1))\)|hsla\(\s*((\d{1,2}|[1-2]\d{2}|3([0-5]\d|60)))\s*,\s*((\d{1,2}|100)\s*%)\s*,\s*((\d{1,2}|100)\s*%)(,\s*(0\.\d+|1))\)|rgb\(((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*,\s*){2}((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*)|hsl\(\s*((\d{1,2}|[1-2]\d{2}|3([0-5]\d|60)))\s*,\s*((\d{1,2}|100)\s*%)\s*,\s*((\d{1,2}|100)\s*%)\))$/i', $color)) {
    $result=$color;
  } else {
	$color = '#' . $color;
	if (preg_match('/^(\#[\da-f]{3}|\#[\da-f]{6}|rgba\(((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*,\s*){2}((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*)(,\s*(0\.\d+|1))\)|hsla\(\s*((\d{1,2}|[1-2]\d{2}|3([0-5]\d|60)))\s*,\s*((\d{1,2}|100)\s*%)\s*,\s*((\d{1,2}|100)\s*%)(,\s*(0\.\d+|1))\)|rgb\(((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*,\s*){2}((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*)|hsl\(\s*((\d{1,2}|[1-2]\d{2}|3([0-5]\d|60)))\s*,\s*((\d{1,2}|100)\s*%)\s*,\s*((\d{1,2}|100)\s*%)\))$/i', $color)) {
		$result=$color;
	 }
  }
  return $result;
}

parse_str($_SERVER['QUERY_STRING'], $params);

#$src = isset($params['src']) ? urldecode($params['src']) : '';
$identifier = isset($params['identifier']) ? $params['identifier'] : $params['id'];
$src = "/data/project/wikipathways2wiki/www/js/public/" . $identifier . ".svg";
#$version = isset($params['version']) ? $params['version'] : isset($params['rev']) ? $params['rev'] : 0;

# NOTE: we convert any query params that still use the old highlighter format
#       to the new format in order to maintain backwards compatibility.
$labelOrLabels = isset($params['label']) ? $params['label'] : null;
$xrefOrXrefs = isset($params['xref']) ? $params['xref'] : null;
$colorString = isset($params['colors']) ? $params['colors'] : null;

if ((!is_null($labelOrLabels) || !is_null($xrefOrXrefs)) && !is_null($colorString)) {
	if (!is_null($labelOrLabels)) {
		$labels = array();
		if (is_array($labelOrLabels)) {
			foreach ($labelOrLabels as $label) {
				array_push($labels, $label);
			}
		} else {
			array_push($labels, $labelOrLabels);
		}
	}


	if (!is_null($xrefOrXrefs)){
		$xrefs = array();
		if (is_array($xrefOrXrefs)){
			foreach ($xrefOrXrefs as $xref) {
				array_push($xrefs, $xref);
			}
		} else {
			array_push($xrefs, $xrefOrXrefs);
		}
	}

	$selectors = array();
	foreach ($labels as $label) {
		array_push($selectors, $label);
	}
	foreach ($xrefs as $xref) {
		$xrefParts = explode(",", $xref);
		$dbId = $xrefParts[0];
		$dbName = $xrefParts[1];
		array_push($selectors, $dbName . ":" . $dbId);
	}

	$colors = explode(",",$colorString);
	if (count($selectors) != count($colors)) {
		// if color list is not the same length as selector list, then just use first color
		$firstColor = $colors[0];
		$params[$firstColor] = join(",", $selectors);
	} else {
		for($i=0; $i <count($selectors); $i++){
			$params[$colors[$i]] = $selectors[$i];
		}
	}

}
$paramString = (count($params) == 0 ? "" : ("?" . http_build_query($params))) ;
//header("Content-Type: $type; charset=utf-8");

print '<html>';
print '<body>';
//echo CssParser::hex('white');

print '<style>';
print 'svg.Diagram {';
//print 'border: solid thin black;';
print 'width: 100%;';
print 'height: 100%;';
print '}';
print '</style>';

print '<style>';
foreach ($params as $key => $value) {
	$validated_color=validate_html_color($key);
	if ($validated_color) {
		$selectors = explode(",", $value);
		foreach ($selectors as $selector) {
			print '#'.$selector.' .Icon, .'.$selector.' .Icon, [name="'.$selector.'"] .Icon { fill: '.$validated_color.';}';
			print '#'.$selector.'.Edge path, .'.$selector.'.Edge path { stroke: '.$validated_color.';}';
		}
	}
}
print '</style>';

// SVG must come from wikimedia uploads.
preg_match('/^WP\d+$/', $identifier, $matches);
if (count($matches) == 1) {
	readfile_chunked($src);
	#readfile($src, 200);
#	if(file_exists($src)) {
#		#header('Content-Description: File Transfer');
#		#header('Content-Type: application/octet-stream');
#		#header('Content-Disposition: attachment; filename="mytitle"');
#		#header('Content-Transfer-Encoding: binary');
#		#header('Expires: 0');
#		#header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
#		header('Pragma: public');
#		header('Content-Length: ' . filesize($src));
#		ob_clean();
#		ob_end_flush();
#		$handle = fopen($src, "rb");
#		while (!feof($handle)) {
#			echo fread($handle, 500);
#		}
#	}

	print '<script src="./svg-pan-zoom.min.js"></script>';
	print '<script>svgPanZoom("svg")</script>';
} else {
	print '<p>id must match WP ID, e.g., WP554.</p>';
}

print '</body>';
print '</html>';
?>
