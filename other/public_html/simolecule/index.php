<?php
header( 'Content-type: image/svg+xml' );

parse_str($_SERVER['QUERY_STRING'], $params);

$simoleculeBase = "https://www.simolecule.com/cdkdepict/depict/bow/svg";
$paramString = (count($params) == 0 ? "" : ("?" . http_build_query($params))) ;
$url = $simoleculeBase . $paramString;

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
$svg = curl_exec($ch);
print $svg;
?>
