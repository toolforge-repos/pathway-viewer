from django.http import HttpResponse
from django.shortcuts import render
import re
import webcolors
import requests
import re


CSS3_NAMES_TO_HEX = webcolors.CSS3_NAMES_TO_HEX
THEME_FILENAME_PARTS_BY_THEME = dict(plain='', dark='.dark')

WPRE = re.compile('WP\d+')
NON_ALPHANUMERIC_DASH_RE = re.compile(r"[^A-Za-z0-9\-]")

base_style = '''
<style>
svg.Diagram {
  width: 100%;
  height: 100%;
}
</style>
''';

def index(request):
    theme_filename_part = ''
    theme = request.GET.get('theme', 'plain')
    if theme in THEME_FILENAME_PARTS_BY_THEME:
        theme_filename_part = THEME_FILENAME_PARTS_BY_THEME[theme]
    else:
        theme_filename_part = ''

    wpid = request.GET.get('id')
    if not wpid:
        return HttpResponse('<html><body><p>URL query parameter "id" required. Example: "?id=WP554"</p></body></html>')
    elif not WPRE.match(wpid):
        return HttpResponse('<html><body><p>Invalid id - must be of format WP554</p></body></html>')
      
    with open('/data/project/wikipathways2wiki/www/js/public/' + wpid + theme_filename_part + '.svg') as file:
        svg_data = file.read()
        highlight_style = '<style>'
        for key, value in request.GET.items():
            validated_color = False
            if key in CSS3_NAMES_TO_HEX:
                validated_color = key
            elif key[0] == '#':
                try:
                    normalized = webcolors.normalize_hex(key)
                except ValueError:
                    normalized = False
                if normalized:
                    validated_color = normalized
            else:
                key_len = len(key)
                if key_len == 3 or key_len == 6:
                    normalized = ''
                    try:
                        normalized = webcolors.normalize_hex('#' + key)
                    except ValueError:
                        normalized = False
                    if normalized:
                        validated_color = normalized
    
            if validated_color:
                for raw_selector in value.split(","):
                    # The sub and find steps are to guard against malicious input.
                    # If the selector, with any characters other than alphanumeric
                    # or dash converted to unscores, are not already in the SVG,
                    # we don't include that selector.

                    selector = NON_ALPHANUMERIC_DASH_RE.sub("_", raw_selector)

                    if svg_data.find(selector) > -1:
                        highlight_style += ('#' + selector + ' .Icon, .' + selector + ' .Icon, [name="' + selector + '"] .Icon { fill: ' + validated_color + ';}\n')
                        highlight_style += ('#' + selector + '.Edge path, .' + selector + '.Edge path { stroke: ' + validated_color + ';}\n')
    
        highlight_style += '</style>'

        return HttpResponse('<html><body>' + base_style + highlight_style + '\n' + svg_data + '\n<script src="/pathway-viewer/static/svg-pan-zoom.min.js"></script><script>svgPanZoom("svg")</script></body></html>')

# This is just a proxy for simolecule. The tools.wmflabs.org server security settings don't
# allow loading content from another domain name, so this proxy is needed to allow
# for loading the metabolite structure SVGs from the simolecule.com domain.
def simolecule(request):
    simolecule_base = "https://www.simolecule.com/cdkdepict/depict/bow/svg"
    param_string = "?" + request.GET.urlencode()
    url = simolecule_base + param_string
    r = requests.get(url)
    svg = r.text
    return HttpResponse(svg)
